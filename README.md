# Shop

Zadanie rekrutacyjne do Unity Group - Proporcjonalny podział rabatu

## Opis
Aplikacja pozwala na zdefiniowanie do 5 produktów wraz z cenami oraz kwoty rabatu. Działanie aplikacji polega na proporcjonalnym rozdziale wartości rabatu na wszystkie produkty. W przypadku, kiedy nie uda się rabatu rozdzielić w całości, pozostała część jest dodawana do ostatniego produktu.

## Specyfikacja
Projekt ten jest projektem Mavenowym.
W celu uruchomienia należy projekt skompilować - `mvn compile`, a następnie można uruchomić aplikację z funkcji main w klasie Main: `mvn exec:java -Dexec.mainClass="com.shop.Main"`. Alternatywnie można skompilować aplikację poprzez `javac com.shop.Main` i uruchomić za pomocą `java com.shop.Main`.

Jest to aplikacja konsolowa - dane wejściowe należy definiować "na sztywno" w kodzie (produkty w liście `com.shop.Main.PRODUCTS`, a rabat w `com.shop.Main.TOTAL_DISCOUNT`).

Wynik działania aplikacji prezentowany jest w formie tekstowej w konsoli (terminalu).
