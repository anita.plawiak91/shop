package com.shop;

import org.junit.Assert;
import org.junit.Test;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;


public class DiscountCalculatorTest {

    private DiscountCalculator calculator = new DiscountCalculator();

    private final Product product1 = new Product("glasses", new BigDecimal(500));
    private final Product product2 = new Product("laptop", new BigDecimal(1500));
    private final Product product3 = new Product("happiness", new BigDecimal(-10));
    private final Product product4 = new Product("book", new BigDecimal(20));
    private final Product product5 = new Product("ring", new BigDecimal(300));
    private final Product product6 = new Product("mouse", new BigDecimal(100));
    private final Product product7 = new Product("headphones", new BigDecimal(100));
    private final Product product8 = new Product("wallet", new BigDecimal(100));


    private List<Product> createProductsList(Product... products) {
        return Arrays.asList(products);
    }


    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForInvalidProducts() {
        //given
        List<Product> products = createProductsList(product1, product2, product3);
        BigDecimal discount = new BigDecimal(100);

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        //exception thrown
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForEmptyListOfProducts() {
        //given
        List<Product> products = createProductsList();
        BigDecimal discount = new BigDecimal(100);

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        //exception thrown
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForTooManyProducts() {
        //given
        List<Product> products = createProductsList(product1, product2, product3, product4, product5, product1);
        BigDecimal discount = new BigDecimal("100.00");

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        //exception thrown
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionForTotalDiscountBiggerThanSumOfProductsPrices() {
        //given
        List<Product> products = createProductsList(product1, product2, product4);
        BigDecimal discount = new BigDecimal("2021.00");

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        //exception thrown
    }

    @Test
    public void shouldCalculateDiscountForTwoValidProducts() {
        //given
        List<Product> products = createProductsList(product1, product2);
        BigDecimal discount = new BigDecimal(100);

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        Assert.assertEquals(productsWithDiscounts.size(), 2);
        Assert.assertEquals(new BigDecimal("25.00"), productsWithDiscounts.get(0).getDiscount());
        Assert.assertEquals(new BigDecimal("75.00"), productsWithDiscounts.get(1).getDiscount());

    }

    @Test
    public void shouldCalculateDiscountForTreeProductsWithTheSamePrice() {
        //given
        List<Product> products = createProductsList(product6, product7, product8);
        BigDecimal discount = new BigDecimal("100.00");

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        Assert.assertEquals(productsWithDiscounts.size(), 3);
        Assert.assertEquals(new BigDecimal("33.33"), productsWithDiscounts.get(0).getDiscount());
        Assert.assertEquals(new BigDecimal("33.33"), productsWithDiscounts.get(1).getDiscount());
        Assert.assertEquals(new BigDecimal("33.34"), productsWithDiscounts.get(2).getDiscount());

    }

    @Test
    public void shouldCalculateDiscountForTotalDiscountZERO() {
        //given
        List<Product> products = createProductsList(product6, product7, product8);
        BigDecimal discount = new BigDecimal("0.00");

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        Assert.assertEquals(productsWithDiscounts.size(), 3);
        Assert.assertEquals(new BigDecimal("0.00"), productsWithDiscounts.get(0).getDiscount());
        Assert.assertEquals(new BigDecimal("0.00"), productsWithDiscounts.get(1).getDiscount());
        Assert.assertEquals(new BigDecimal("0.00"), productsWithDiscounts.get(2).getDiscount());

    }

    @Test
    public void shouldCalculateDiscountForTotalDiscountEqualToSumOfProductsPrices() {
        //given
        List<Product> products = createProductsList(product1, product2, product4);
        BigDecimal discount = new BigDecimal("2020.00");

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        Assert.assertEquals(productsWithDiscounts.size(), 3);
        Assert.assertEquals(new BigDecimal("500.00"), productsWithDiscounts.get(0).getDiscount());
        Assert.assertEquals(new BigDecimal("1500.00"), productsWithDiscounts.get(1).getDiscount());
        Assert.assertEquals(new BigDecimal("20.00"), productsWithDiscounts.get(2).getDiscount());
    }

    @Test
    public void shouldCalculateDiscountWhenTotalDiscountIsReallySmall() {
        //given
        List<Product> products = createProductsList(product4, product5, product6);
        BigDecimal discount = new BigDecimal("0.01");

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        Assert.assertEquals(productsWithDiscounts.size(), 3);
        Assert.assertEquals(new BigDecimal("0.00"), productsWithDiscounts.get(0).getDiscount());
        Assert.assertEquals(new BigDecimal("0.00"), productsWithDiscounts.get(1).getDiscount());
        Assert.assertEquals(new BigDecimal("0.01"), productsWithDiscounts.get(2).getDiscount());
    }

    @Test
    public void shouldCalculateDiscountWhenOnePriceIsBiggerThanSumOfOtherProducts() {
        //given
        List<Product> products = createProductsList(product4, product5, product6);
        BigDecimal discount = new BigDecimal("0.02");

        //when
        List<Product> productsWithDiscounts = calculator.calculateDiscountsForProducts(products, discount);

        //then
        Assert.assertEquals(productsWithDiscounts.size(), 3);
        Assert.assertEquals(new BigDecimal("0.00"), productsWithDiscounts.get(0).getDiscount());
        Assert.assertEquals(new BigDecimal("0.01"), productsWithDiscounts.get(1).getDiscount());
        Assert.assertEquals(new BigDecimal("0.01"), productsWithDiscounts.get(2).getDiscount());
    }

}
