package com.shop;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    private static final List<Product> PRODUCTS = Arrays.asList(
            new Product("watch", new BigDecimal("500.00")),
            new Product("smartphone", new BigDecimal("1500.00"))
    );

    private static final BigDecimal TOTAL_DISCOUNT = new BigDecimal("100.00");

    public static void main(String[] args) {

        DiscountCalculator calculation = new DiscountCalculator();
        calculation.calculateDiscountsForProducts(PRODUCTS, TOTAL_DISCOUNT);

        System.out.println("For total discount " + TOTAL_DISCOUNT + ", ");

        String resultMessage = PRODUCTS.stream()
                .map(Main::formatMessage)
                .collect(Collectors.joining(",\n"));

        System.out.println(resultMessage + ".");
    }

    private static String formatMessage(Product product) {
        return product.getName()
                + " got discounted from " + product.getPrice()
                + " by " + product.getDiscount();
    }
}
