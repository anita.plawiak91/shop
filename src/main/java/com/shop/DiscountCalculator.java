package com.shop;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

public class DiscountCalculator {

    public List<Product> calculateDiscountsForProducts(List<Product> products, BigDecimal totalDiscount) {
        validateInitialData(products, totalDiscount);
        BigDecimal totalPrice = calculateTotalPriceForAllProducts(products);

        Iterator<Product> iterator = products.iterator();
        Product product = iterator.next();
        while (iterator.hasNext()) {
            BigDecimal currentDiscount = calculateDiscountForEveryProductWithoutLastOne(totalDiscount, totalPrice, product);
            product.setDiscount(currentDiscount);
            product = iterator.next();
        }
        BigDecimal currentDiscount = calculateDiscountForLastProduct(totalDiscount, products);
        product.setDiscount(currentDiscount);

        return products;
    }

    private BigDecimal calculateTotalPriceForAllProducts(List<Product> products) {
        return products.stream()
                .map(Product::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private BigDecimal calculateDiscountForEveryProductWithoutLastOne(BigDecimal totalDiscount, BigDecimal totalPrice, Product product) {
        BigDecimal productPrice = product.getPrice();
        return productPrice
                .multiply(totalDiscount)
                .divide(totalPrice, 2, BigDecimal.ROUND_FLOOR);
    }

    private BigDecimal calculateDiscountForLastProduct(BigDecimal totalDiscount, List<Product> products) {
        BigDecimal currentSumOfDiscounts = getCurrentSumOfDiscounts(products);
        return totalDiscount.subtract(currentSumOfDiscounts);
    }

    private BigDecimal getCurrentSumOfDiscounts(List<Product> products) {
        return products.stream()
                .map(Product::getDiscount)
                .filter(Objects::nonNull)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    private void validateInitialData(List<Product> products, BigDecimal totalDiscount) {
        validateDoesNotContainNullValues(products, totalDiscount);
        validateCountOfProducts(products);
        validatePricesOfProducts(products);

        BigDecimal totalPrice = calculateTotalPriceForAllProducts(products);
        isDiscountValid(totalPrice, totalDiscount);
    }

    private void validateDoesNotContainNullValues(List<Product> products, BigDecimal totalDiscount) {
        boolean isNotValid = products == null || listContainsNullValues(products) || totalDiscount == null;
        if (isNotValid) {
            throw new IllegalArgumentException("Initial data contains null values.");
        }
    }

    private boolean listContainsNullValues(List<Product> products) {
        return products.stream().anyMatch(Objects::isNull);
    }

    private void isDiscountValid(BigDecimal totalPrice, BigDecimal totalDiscount) {
        if (totalPrice.compareTo(totalDiscount) < 0) {
            throw new IllegalArgumentException("Discount is bigger than total of products prices!");
        }
    }

    private void validatePricesOfProducts(List<Product> products) {
        for (Product product : products) {
            if (hasNotPositivePrice(product)) {
                throw new IllegalArgumentException("Product's price is not valid!");
            }
        }
    }

    private boolean hasNotPositivePrice(Product product) {
        return !(product.getPrice().compareTo(BigDecimal.ZERO) > 0);
    }

    private void validateCountOfProducts(List<Product> products) {
        int productsNumber = products.size();
        if (productsNumber > 5) {
            throw new IllegalArgumentException("Expected number of products is 1 to 5. " + productsNumber + " products received");
        }
        if (productsNumber == 0) {
            throw new IllegalArgumentException("List of products is empty!");
        }
    }
}